#!/usr/bin/env bash

PLUGINS+=( coc-python )
PLUGINS+=( coc-java )
PLUGINS+=( coc-texlab )
PLUGINS+=( coc-yaml )
PLUGINS+=( coc-omnisharp )
PLUGINS+=( coc-json )
PLUGINS+=( coc-tslint )
PLUGINS+=( coc-tsserver )
PLUGINS+=( coc-rls )
PLUGINS+=( coc-rust-analyzer )

function run() {
    echo "${*}"
    "${@}"
    return $?
}

for plug in ${PLUGINS[@]}; do
    run nvim --headless +"CocInstall -sync ${plug}" +qall
    [[ $? -ne 0 ]] && echo "Warning: Failed install for ${plug}"
done
