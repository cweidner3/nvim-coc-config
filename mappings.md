
**Local Leader**: `<SPACE>`

## Surround Code

TBD...

## (Un)Commenting

+-------------------------+-------------------------------------+
| Key                     | What                                |
+=========================+=====================================+
| gcc                     | Comment line of code                |
+-------------------------+-------------------------------------+
| gc<direction>           | Comment with direction element      |
+-------------------------+-------------------------------------+
| gcgc                    | Uncomments adjacent commented lines |
+-------------------------+-------------------------------------+
| (visual) gc             | Comment out visual block            |
+-------------------------+-------------------------------------+
| :g/<pattern>/Commentary | Comment everythign with the pattern |
+-------------------------+-------------------------------------+

## File Navigation

+------+--------------------------------+
| Key  | What                           |
+======+================================+
| <F2> | Toggle File Tree menu          |
+------+--------------------------------+
| i    | Open on top of current window  |
+------+--------------------------------+
| s    | Open to left of current window |
+------+--------------------------------+
| I    | Toggle hidden files            |
+------+--------------------------------+
| u    | Move up a directory            |
+------+--------------------------------+
| C    | Move into a directory          |
+------+--------------------------------+
| ?    | Toggle Help text               |
+------+--------------------------------+

## Multiple Cursors

TBD...

## Tables

TBD...

## Align

TBD...

## Coc Stuffs

TBD...
