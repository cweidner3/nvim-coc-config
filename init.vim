set nocompatible

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(stdpath('data') . '/plugged')

Plug 'cweidner3/editor-conf-vim'

" Framework for Language Server Support
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Colorschemes
Plug 'morhetz/gruvbox'

" Syntax highlight plugins
Plug 'LnL7/vim-nix'
Plug 'plasticboy/vim-markdown'
Plug 'neovimhaskell/haskell-vim'
Plug 'elixir-editors/vim-elixir'
Plug 'cespare/vim-toml'
Plug 'kergoth/vim-bitbake'
Plug 'mfukar/robotframework-vim'
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx' " javascript react
Plug 'matze/vim-lilypond'
Plug 'gianarb/vim-flux'
Plug 'craigmac/vim-mermaid'

" Allows for file navigation within vim
Plug 'preservim/nerdtree'

" Indexes code stuff for quick nav on the side
Plug 'preservim/tagbar'

" Shows dashed line for each soft indent
Plug 'Yggdroot/indentLine'

" Allow fancy editing with multiple cursors
Plug 'terryma/vim-multiple-cursors'

" Allow for encapuslating text with special characters
Plug 'tpope/vim-surround'
" To aid in (un)commenting code
Plug 'tpope/vim-commentary'

" Auto whitespace cleanup and display
Plug 'ntpeters/vim-better-whitespace'

" Edit tables more easily and let TM do the formatting
Plug 'dhruvasagar/vim-table-mode'

" Helper for aligning code
Plug 'junegunn/vim-easy-align'

" Show css color codes as colors
Plug 'ap/vim-css-color'

" Make using the f action easier in vim
Plug 'rhysd/clever-f.vim'

" RestructuredText thing...
Plug 'gu-fan/InstantRst'

" Show git changes
"
"   <https://jakobgm.com/posts/vim/git-integration/>
"
" For more features:
" - jreybert/vimagit
"
Plug 'airblade/vim-gitgutter'

Plug 'lervag/vimtex'

call plug#end()


filetype indent plugin on

" For the special characters
scriptencoding utf-8
set encoding=utf-8

set exrc   " Allow local vim files
set secure " Prevent autocmd in local vim files

if has('mouse')
  set mouse=a
endif

syntax enable
"set t_Co=256 " If avaliable

set backspace=indent,eol,start
set ruler
set number
set showcmd
set incsearch
set modeline

set ignorecase
set smartcase
set colorcolumn=80
set lcs=tab:\·\·,trail:#
set cursorline
set nowrap

set autoindent
set hlsearch
set shiftwidth=4
set tabstop=4
set expandtab
set nojoinspaces " disables two spaces after period when using gq
set foldlevel=99 " for when we turn on folding

" == Set Local Leader

nnoremap <SPACE> <Nop>
let mapleader=' '

" == Coc Language Server Extensions
"
" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions
"

"    \ 'coc-spell-checker',
let g:coc_global_extensions=[
    \ 'coc-sh',
    \ 'coc-cmake',
    \ 'coc-java',
    \ 'coc-json',
    \ 'coc-tslint',
    \ 'coc-tsserver',
    \ 'coc-prisma',
    \ 'coc-diagnostic',
    \ 'coc-pyright',
    \ 'coc-rls',
    \ 'coc-rust-analyzer',
    \ 'coc-yaml',
    \ 'coc-omnisharp',
    \ 'coc-flutter',
    \ 'coc-webview',
    \ 'coc-xml',
    \ 'coc-markdown-preview-enhanced'
    \]

let g:coc_disable_startup_warning = 1

" == Filetype detection overrides

au BufNewFile,BufRead *.resource set filetype=robot

" == Custom Commands

" Clear trailing spaces
command -nargs=0 Cts %s/\s\+$//e

command -nargs=0 VCS /\v^(\<|\>|\=){7}

" == NT & TB Key Mapping
" See :help NERDTree.txt
" See :help tagbar.txt

nnoremap <F2> :NERDTreeToggle<CR>
vnoremap <F2> <ESC>:NERDTreeToggle<CR>
inoremap <F2> <ESC>:NERDTreeToggle<CR>

nnoremap <F3> :TagbarToggle<CR>
vnoremap <F3> <ESC>:TagbarToggle<CR>
inoremap <F3> <ESC>:TagbarToggle<CR>

" == GIT

nnoremap <leader>gl :GitGutterLineHighlightsToggle<CR>
nnoremap <leader>gn :GitGutterNextHunk<CR>
nnoremap <leader>gp :GitGutterPrevHunk<CR>
nnoremap <leader>gs :GitGutterStageHunk<CR>
nnoremap <leader>gu :GitGutterUndoHunk<CR>

" == Indent Line
" See :help indentLine.txt

let g:indentLine_setColors=1
let g:indentLine_concealcursor='nv' " (n) Normal; (v) Visual; (i) Insert; (c) CmdLine
let g:indentLine_conceallevel=2
let g:indentLine_fileTypeExclude=['markdown']

" == Better Whitespace
" See :help better-whitespace.txt

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" To disable auto-strip for these filetypes
let g:better_whitespace_filetypes_blacklist=[
	\ 'diff', 'gitcommit', 'unite', 'qf', 'help', 'markdown'
	\]

" == Clever F
" See :help clever-f.txt

" (1) f and F always move in their appropriate direction
" (0) f Continues in starting direction, F reverses course
let g:clever_f_fix_key_direction=0

" == Markdown Plugin
" See :help vim-markdown

let g:vim_markdown_folding_disabled=1
let g:vim_markdown_auto_insert_bullets=0

" == Haskell
" See :help haskell-vim.txt

let g:haskell_enable_quantification=1
let g:haskell_enable_recursivedo=1
let g:haskell_enable_arrowsyntax=1
let g:haskell_enable_pattern_synonyms=1
let g:haskell_enable_typeroles=1
let g:haskell_enable_static_pointers=1

let g:haskell_classic_highlighting=0
let g:haskell_disable_TH=0

let g:haskell_vim_indent_if=2
let g:haskell_vim_indent_case=2
let g:haskell_vim_indent_let=2
let g:haskell_vim_indent_where=2
let g:haskell_vim_indent_before_where=2
let g:haskell_vim_indent_after_bare_where=2
let g:haskell_vim_indent_do=2
let g:haskell_vim_indent_in=2
let g:haskell_vim_indent_guard=2

let g:cabal_indent_section=2

" == Easy Align

xmap <leader>aa <Plug>(EasyAlign)
nmap <leader>aa <Plug>(EasyAlign)

xnoremap <leader>as :EasyAlign /\s\s\+/<CR>
nnoremap <leader>as :EasyAlign /\s\s\+/<CR>

" == Table Mode

" Show current status of these globals
command -nargs=0 TMstatus
    \ let g:table_mode_separator
    \ | let g:table_mode_corner
    \ | let g:table_mode_corner_corner
    \ | let g:table_mode_header_fillchar
    \ | let g:table_mode_fillchar
    \ | let g:table_mode_map_prefix
    \ | let b:table_mode_corner
    \ | let b:table_mode_corner_corner
    \ | let b:table_mode_header_fillchar
" Command for TableMode Emacs Style
command -nargs=0 TMem
    \ let g:table_mode_separator="|"
    \ | let g:table_mode_corner='+'
    \ | let g:table_mode_corner_corner='+'
    \ | let g:table_mode_header_fillchar='='
    \ | unlet b:table_mode_corner
    \ | unlet b:table_mode_corner_corner
    \ | unlet b:table_mode_header_fillchar
" Command for TableMode Bitbucket Markdown Style
command -nargs=0 TMbb
    \ let g:table_mode_separator="|"
    \ | let g:table_mode_corner='|'
    \ | let g:table_mode_corner_corner='|'
    \ | let g:table_mode_header_fillchar='-'
    \ | unlet b:table_mode_corner
    \ | unlet b:table_mode_corner_corner
    \ | unlet b:table_mode_header_fillchar

" Default mode for table mode
"
"let g:table_mode_separator="|"
"let g:table_mode_corner='+'
"let g:table_mode_corner_corner='+'
"let g:table_mode_header_fillchar='='

" Colorscheme
colorscheme gruvbox

" == TexLab

" Specify that we want to handle latex vs tex
let g:tex_flavor = 'latex'

" == COC.NVIM Configuration

" Settings to ensure coc works well
set hidden " Required so things don't fail
set nobackup " Some servers have issues with backup files, see #649
set nowritebackup " Some servers have issues with backup files, see #649
set cmdheight=2 " for displaying messages
set updatetime=300 " Speed things up a bit
set shortmess+=c " don't give \|ins-completion-menu\| messages.
set signcolumn=yes " always show signcolumns

let g:WorkspaceFolders=1    " Allow storing workspace folders
set sessionoptions+=globals  " Allow restoring workspace folders

" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
" Add manual code completion
inoremap <silent><expr> <c-space> coc#refresh()

" Use <Tab> <S-Tab> to naviage completion list
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

" Use <C-e> and <C-y> to cancel and confirm completion: >
"inoremap <silent><expr> <C-e> coc#pum#visible() ? coc#pum#cancel() : "\<C-e>"
"inoremap <silent><expr> <C-y> coc#pum#visible() ? coc#pum#confirm() : "\<C-y>"
inoremap <silent><expr> <C-CR> coc#pum#visible() ? coc#pum#confirm() : "\<C-CR>"

" Diagnostics Navigation
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)
" Remap keys for gotos
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
"autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
"nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)

"augroup mygroup
"  autocmd!
"  " Setup formatexpr specified filetype(s).
"  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
"  " Update signature help on jump placeholder
"  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
"augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>l  <Plug>(coc-codeaction-selected)
nmap <leader>l  <Plug>(coc-codeaction-selected)
" Remap for do codeAction of current line
nmap <leader>lc  <Plug>(coc-codeaction)
"" Fix autofix problem of current line
xmap <leader>lf  <Plug>(coc-format-selected)
nmap <leader>lf  <Plug>(coc-format-selected)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')


function! s:GoToDefinition()
  if CocAction('jumpDefinition')
    return v:true
  endif

  " let ret = execute("silent! normal \<C-]>")
  " if ret =~ "Error" || ret =~ "错误"
  "   call searchdecl(expand('<cword>'))
  " endif
endfunction

nmap <silent> <F4> :call <SID>GoToDefinition()<CR>
